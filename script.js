const quizData = [
    {
        question: 'How old is the earth?',
        a: "4.5 billion years",
        b: "3.2 billion years",
        c: "1 billion years",
        d: "2022 years",
        correct: "a"
    },
    {
        question: 'Which is the most common favorite color to have?',
        a: 'Green',
        b: 'Blue',
        c: 'Pink',
        d: 'Red',
        correct:"b"
    },
    {
        question: 'what is the second most popular food franchise?',
        a:'McDonalds',
        b:'Pizza Hut',
        c:'Burger King',
        d:'KFC',
        correct: "d"
    },
    {
        question: 'What is the most used programming language?',
        a:'JavaScript',
        b:'C#',
        c:'SQL',
        d:'HTML/CSS',
        correct:"a"
    }
];

const quiz = document.getElementById("quiz");
const answersElement = document.querySelectorAll(".answer");
const questionElement = document.getElementById('question');
const a_text = document.getElementById('a_text');
const b_text = document.getElementById('b_text');
const c_text = document.getElementById('c_text');
const d_text = document.getElementById('d_text');
const submitButton = document.getElementById('submit');

let currentQuiz = 0;
let score = 0;


loadQuiz();

function loadQuiz(){
    deselectAnswers();
    const currentQuizData = quizData[currentQuiz];
    questionElement.innerHTML = currentQuizData.question;
    a_text.innerHTML = currentQuizData.a;
    b_text.innerHTML = currentQuizData.b;
    c_text.innerHTML = currentQuizData.c;
    d_text.innerHTML = currentQuizData.d;

}

function getSelectedAnswer(){
 const answersElement = document.querySelectorAll(".answer");

 let answer = undefined;

    answersElement.forEach((answersElement)=>{
     if(answersElement.checked){
         answer = answersElement.id;
     }
 });
    return answer;
}

function deselectAnswers() {
    answersElement.forEach((answersElement) => {
        answersElement.checked = false;
    });
}

submitButton.addEventListener('click',()=> {

    const answer = getSelectedAnswer();

    if(answer){
        if(answer === quizData[currentQuiz].correct){
            score++;
        }
        currentQuiz++;
        if(currentQuiz < quizData.length){
            loadQuiz();
        }else {
            quiz.innerHTML = `
                <h2>You answered correctly at ${score}/${quizData.length} questions.</h2>
                
                <button onclick="location.reload()">Restart</button>
            `;
        }
    }

});